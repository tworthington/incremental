* Incremental Backups
Just a =bash= script to do incremental backups by day, week, and month. It was written because I have used similar systems in the past which suffered from uncaught bitrot in the base snapshot from which the increments diverged. Thus, in 12 weeks of backups the corrupted file was corrupted in every backup.

The script is a compromise - daily backups are incremental in the same sense as above: a base backup and then a set of deltas. But weekly backups are independent copies of the current daily backup.

The daily backups are intended as protection against mistaken changes or loss on the primary source. The weeklies are primarily aimed at protecting against bitrot on the backups themselves and are thus independent of each other.

** Usage
   =incremental <protect> <safety> [dailies [weeklies [monthlies [weekend]]]]=
   - =protect= is the location you want to back up. This must be a directory. Currently defaults to =/sharedfiles= but obviously most people will want to change this.
   - =safety= is the location you want to store the backups in. This must be a directory. Currently defaults to =/safe/outsource= but obviously most people will want to change this.
   - =dailies= is the number of dalies to keep. This defaults to 7 and can not be lower than 1.
   - =weeklies= is the number of weekly backups to keep. Default is 4.
   - =monthlies= is the number of monthly backups to keep. Default is 6.
   - =weekend= is the day of the week (from 1=Monday to 7=Sunda) on which weeklies and monthlies are made.

** Operation
*** Normal usage
    When run, the script will take a linked copy (i.e., the "copy" will consist entirely of hard links, at least as far as the normal files are concerned) of the previous day's backup. It will then update that snapshot with the changes that have accumulated in the source directory tree.

    If it is the weekend, as defined by the cli parameter or the default of Sunday, then it will also take a completely independent copy of today's daily backup making new files rather than links.

    If it is the last weekend of the month, then it will also take a linked copy of that weekly copy as the monthly backup. If weeklies are not being taken, then the monthly will be a full backup from source.

    If the script is run more than once per day, then the source is simply re-synced to today's backup.

*** First use
    On first execution, the script asks for confirmation. It then takes an exact copy of the protected directory tree as the starting point. It takes a second, completely independent, copy as the first weekly backup. A linked copy of that then becomes the first monthly (assuming weekly and monthly backups are being used).

    This means that, if monthlies are set to 6, then the snapshot taken on the first day will be available for 6 months.
